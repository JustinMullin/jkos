package com.jmullin
package jkos

import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}

object Main extends App {
  val cfg = new LwjglApplicationConfiguration
  cfg.title = "Jkos"
  cfg.height = 900
  cfg.width = 800
  Jkos.desktop = true
  cfg.forceExit = false
  new LwjglApplication(new Jkos, cfg)
}
