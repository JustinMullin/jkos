package com.jmullin
package jkos

import com.badlogic.gdx.Game

object Jkos {
  var desktop = false
}

class Jkos(output: KeyboardOutput = null) extends Game {
  if(output != null) {
    Controller.keyboardOutput = output
  }

  override def create(): Unit = {
    Assets.load()
    Assets.finishLoading()
    Assets.populate()

    setScreen(new MainScreen)
  }
}
