package com.jmullin.jkos

import com.badlogic.gdx.graphics.g2d.{BitmapFont, TextureAtlas, Sprite}
import com.jmullin.drifter.assets.DrifterAssets

object Assets extends DrifterAssets {
  var mainAtlas: TextureAtlas = _

  var xoloniumSmall: BitmapFont = _
  var xolonium: BitmapFont = _

  var fill: Sprite = _
  var buttonBorder: Sprite = _
  var buttonFill: Sprite = _
}