package com.jmullin
package jkos.entity

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle
import com.badlogic.gdx.scenes.scene2d.ui.{Label, Table}
import com.badlogic.gdx.scenes.scene2d.utils.Align
import com.jmullin.jkos.{Jkos, Controller, Assets, Chords}

object Keyboard extends Table {
  var buttons = Set[Button]()

  import Chords._

  setFillParent(true)
  padLeft(10).padRight(10)
  padBottom(5).padTop(0)

  val leftKeys = new Table
  val specialKeys = new Table
  val rightKeys = new Table

  add(leftKeys)
  add(specialKeys).expandX.fillX
  add(rightKeys)

  val leftIds = List(LeftA, LeftAB, LeftB, LeftBC, LeftC)
  val rightIds = List(RightA, RightAB, RightB, RightBC, RightC)

  val scale = if(Jkos.desktop) 1 else 2
  val (w, h) = (100*scale, 90*scale)

  for(i <- 0 until 5) {
    leftKeys.add(new Button(leftIds(i), false, true)).size(w, h).row
    rightKeys.add(new Button(rightIds(i), true, true)).size(w, h).row
  }

  specialKeys.add(new Button(LeftZ, false)).size(w, h)
  specialKeys.add.expandX
  specialKeys.add(new Button(RightZ, true)).size(w, h)

  specialKeys.row()

  specialKeys.add(new Button(LeftMeta, false)).size(w, h*4)
  val modeLabel = new Label("abc", new LabelStyle(Assets.xolonium, Color.WHITE))
  specialKeys.add(modeLabel).expandX.align(Align.bottom).padBottom(24)
  specialKeys.add(new Button(RightMeta, true)).size(w, h*4)

  bottom()

  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    Controller.tick(Gdx.graphics.getDeltaTime)
    super.draw(batch, parentAlpha)
  }

  def clearDragged(): Unit = {
    for(button <- buttons; if button.draggedTo) {
      button.down = false
      button.draggedTo = false
      button.doUp()
    }
  }
}
