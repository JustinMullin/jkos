package com.jmullin
package jkos.entity

import com.badlogic.gdx.graphics.g2d.{SpriteBatch, Batch}
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds
import com.badlogic.gdx.scenes.scene2d.{Actor, InputEvent, InputListener}
import com.jmullin.drifter.{Draw, Entity}
import com.jmullin.jkos.Chords._
import com.jmullin.jkos.Controller.{LowerAlpha, Mode}
import com.jmullin.jkos.{KeyboardShader, Assets, Controller}

class Button(id: KeyID, right: Boolean, primary: Boolean = false) extends Entity {
  Keyboard.buttons += this

  val smallFont = Assets.xoloniumSmall
  val font = Assets.xolonium
  var label = ""
  var (labelA, labelB, labelC) = ("", "", "")

  var (offset, offsetA, offsetB, offsetC) = (
    new TextBounds(), new TextBounds(), new TextBounds(), new TextBounds()
    )

  refreshLabel(LowerAlpha)

  class TouchPulse(var v: V2, var pulse: Float) {
    def update(v: V2, pulse: Float): Unit = {
      this.v.set(v.x, v.y)
      this.pulse = pulse
    }

    def value(p: Float): Unit = {
      pulse = p
    }

    def decay(): Unit = {
      pulse *= 0.9f
    }
  }

  val downPulse = new TouchPulse(origin, 0)
  val upPulse = new TouchPulse(origin, 0)
  var down = false
  var draggedTo = false

  addListener(new InputListener {
    override def touchDown(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Boolean = {
      doDown()
      true
    }

    override def enter(event: InputEvent, x: Float, y: Float, pointer: Int, fromActor: Actor): Unit = {
      if(primary && !Controller.keysDown.contains(id)) {
        doDown()
        draggedTo = true
      }
    }

    override def exit(event: InputEvent, x: Float, y: Float, pointer: Int, toActor: Actor): Unit = {
      if(primary && Controller.keysDown.contains(id) && draggedTo) {
        doUp()
        draggedTo = false
      }
    }

    override def touchUp(event: InputEvent, x: Float, y: Float, pointer: Int, button: Int): Unit = {
      doUp()
      Keyboard.clearDragged()
    }
  })

  def doDown(): Unit = {
    Controller.down(id)
    down = true
    downPulse.update(V2(getParent.getX + getCenterX, getCenterY), 1f)
    upPulse.update(V2(getParent.getX + getCenterX, getCenterY), 1f)
  }

  def doUp(): Unit = {
    Controller.up(id)
    down = false
  }

  override def render(implicit batch: Batch): Unit = {
    implicit val _batch: SpriteBatch = batch.asInstanceOf[SpriteBatch]

    KeyboardShader.on()
    KeyboardShader.pulse.setUniformf("pulseDownV", downPulse.v)
    KeyboardShader.pulse.setUniformf("pulseDownAmount", downPulse.pulse)
    KeyboardShader.pulse.setUniformf("pulseUpV", upPulse.v)
    KeyboardShader.pulse.setUniformf("pulseUpAmount", upPulse.pulse)

    Draw.sprite(Assets.fill, V2(getX, getY), V2(getWidth, getHeight))

    KeyboardShader.off()

    font.draw(batch, label, centerX-offset.width/2, centerY+offset.height/2)

    if(id != LeftMeta && id != RightMeta) {
      smallFont.setColor(1, 1, 1, 0.5f)
      val sign = if(right) -1 else 1
      smallFont.draw(batch, labelA, centerX+sign*offset.width/2+sign*25-offsetA.width/2, centerY+offsetB.height/2+offsetA.height+5)
      smallFont.draw(batch, labelB, centerX+sign*offset.width/2+sign*25-offsetB.width/2, centerY+offsetB.height/2)
      smallFont.draw(batch, labelC, centerX+sign*offset.width/2+sign*25-offsetC.width/2, centerY+offsetB.height/2-offsetC.height-5)
    }

    downPulse.decay()
    upPulse.decay()
    if(down) downPulse.value(1f)
  }

  def centerX = getX + getWidth / 2
  def centerY = getY + getHeight / 2

  def refreshLabel(mode: Mode): Unit = {
    label = id.label(mode.map)
    if(right) {
      labelA = KeySet(id, LeftA).label(mode.map)
      labelB = KeySet(id, LeftB).label(mode.map)
      labelC = KeySet(id, LeftC).label(mode.map)
    } else {
      labelA = KeySet(id, RightA).label(mode.map)
      labelB = KeySet(id, RightB).label(mode.map)
      labelC = KeySet(id, RightC).label(mode.map)
    }

    offset.set(font.getBounds(label))
    offsetA.set(smallFont.getBounds(labelA))
    offsetB.set(smallFont.getBounds(labelB))
    offsetC.set(smallFont.getBounds(labelC))
  }
}
