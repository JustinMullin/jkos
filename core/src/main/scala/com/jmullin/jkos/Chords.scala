package com.jmullin.jkos

object Chords {
  import KeyEvent._

  val CHORD_TIME = 250

  val META_SHIFT = 220
  val META_NUM_SHIFT = 221
  val META_FAST_DELETE = 222
  val META_SWITCH_KEYSET = 223

  type KeyMap = Map[KeySet, Chord]

  case class KeySet(ids: KeyID*) {
    def label(map: KeyMap) = map.find(_._1.ids.toSet == this.ids.toSet).map(_._2.label).getOrElse("")
  }

  class Chord(val label: String)
  case class TextChord(l: String, print: String) extends Chord(l)
  case class MetaChord(l: String, meta: Int) extends Chord(l)

  trait KeyID {
    def label(map: KeyMap) = map.find(_._1 == KeySet(this)).map(_._2.label).getOrElse("")
    //def code = lowercaseChords.find(_._1 == KeySet(this)).map(_._2.print).getOrElse(List())
    override def toString = getClass.getSimpleName.replaceAll("\\$", "")
  }

  object LeftA extends KeyID
  object LeftAB extends KeyID
  object LeftB extends KeyID
  object LeftBC extends KeyID
  object LeftC extends KeyID
  object LeftZ extends KeyID
  object LeftMeta extends KeyID

  object RightA extends KeyID
  object RightAB extends KeyID
  object RightB extends KeyID
  object RightBC extends KeyID
  object RightC extends KeyID
  object RightZ extends KeyID
  object RightMeta extends KeyID

  val globalChords = Map[KeySet, Chord](
    KeySet(LeftB, RightB) -> MetaChord("⇧", META_SHIFT),
    KeySet(LeftMeta, RightMeta) -> MetaChord("Num", META_NUM_SHIFT),
    KeySet(LeftMeta) -> MetaChord("Del", META_FAST_DELETE),
    KeySet(LeftMeta, RightB) -> MetaChord("Del+", META_FAST_DELETE),
    KeySet(RightMeta) -> TextChord("Space", " "),
    KeySet(LeftA, RightA) -> MetaChord("↑", KEYCODE_DPAD_UP),
    KeySet(LeftC, RightC) -> MetaChord("↓", KEYCODE_DPAD_DOWN),
    KeySet(LeftA, RightMeta) -> MetaChord("→", KEYCODE_DPAD_RIGHT),
    KeySet(RightA, LeftMeta) -> MetaChord("←", KEYCODE_DPAD_LEFT),
    KeySet(LeftAB, RightBC) -> TextChord("\\", "\\"),
    KeySet(LeftAB, RightMeta) -> MetaChord("↵", KEYCODE_ENTER),
    KeySet(LeftBC, RightAB) -> TextChord("/", "/"),
    KeySet(LeftAB, RightAB) -> MetaChord("PgDn", KEYCODE_PAGE_UP),
    KeySet(LeftBC, RightBC) -> MetaChord("PgUp", KEYCODE_PAGE_DOWN),

    KeySet(LeftB, LeftMeta) -> MetaChord("SET", META_SWITCH_KEYSET),
    KeySet(RightB, RightMeta) -> MetaChord("SET", META_SWITCH_KEYSET))

  val lowercaseChords = globalChords ++: Map[KeySet, Chord](
    KeySet(LeftA) -> TextChord("a", "a"),
    KeySet(LeftA, RightB) -> TextChord("-", "-"),

    KeySet(LeftB) -> TextChord("b", "b"),
    KeySet(LeftB, RightC) -> TextChord(".", "."),

    KeySet(LeftC) -> TextChord("c", "c"),
    KeySet(LeftC, RightA) -> TextChord("!", "!"),

    KeySet(RightA) -> TextChord("d", "d"),
    KeySet(RightA, LeftB) -> TextChord("'", "'"),

    KeySet(RightB) -> TextChord("e", "e"),
    KeySet(RightB, LeftC) -> TextChord(",", ","),

    KeySet(RightC) -> TextChord("f", "f"),
    KeySet(RightC, LeftA) -> TextChord("?", "?"),

    KeySet(RightAB) -> TextChord("g", "g"),
    KeySet(RightAB, LeftA) -> TextChord("h", "h"),
    KeySet(RightAB, LeftB) -> TextChord("i", "i"),
    KeySet(RightAB, LeftC) -> TextChord("j", "j"),

    KeySet(RightBC) -> TextChord("k", "k"),
    KeySet(RightBC, LeftA) -> TextChord("l", "l"),
    KeySet(RightBC, LeftB) -> TextChord("m", "m"),
    KeySet(RightBC, LeftC) -> TextChord("n", "n"),

    KeySet(LeftAB) -> TextChord("o", "o"),
    KeySet(LeftAB, RightA) -> TextChord("p", "p"),
    KeySet(LeftAB, RightB) -> TextChord("q", "q"),
    KeySet(LeftAB, RightC) -> TextChord("r", "r"),

    KeySet(LeftBC) -> TextChord("s", "s"),
    KeySet(LeftBC, RightA) -> TextChord("t", "t"),
    KeySet(LeftBC, RightB) -> TextChord("u", "u"),
    KeySet(LeftBC, RightC) -> TextChord("v", "v"),

    KeySet(LeftZ) -> TextChord("th", "th"),

    KeySet(RightZ) -> TextChord("w", "w"),
    KeySet(RightZ, LeftA) -> TextChord("x", "x"),
    KeySet(RightZ, LeftB) -> TextChord("y", "y"),
    KeySet(RightZ, LeftC) -> TextChord("z", "z"))

  val uppercaseChords = globalChords ++: Map[KeySet, Chord](
    KeySet(LeftA) -> TextChord("A", "A"),
    KeySet(LeftA, RightB) -> TextChord("_", "_"),

    KeySet(LeftB) -> TextChord("B", "B"),
    KeySet(LeftB, RightC) -> TextChord(":", ":"),

    KeySet(LeftC) -> TextChord("C", "C"),
    KeySet(LeftC, RightA) -> TextChord("|", "|"),

    KeySet(RightA) -> TextChord("D", "D"),
    KeySet(RightA, LeftB) -> TextChord("\"", "\""),

    KeySet(RightB) -> TextChord("E", "E"),
    KeySet(RightB, LeftC) -> TextChord(";", ";"),

    KeySet(RightC) -> TextChord("F", "F"),
    KeySet(RightC, LeftA) -> TextChord("~", "~"),

    KeySet(RightAB) -> TextChord("G", "G"),
    KeySet(RightAB, LeftA) -> TextChord("H", "H"),
    KeySet(RightAB, LeftB) -> TextChord("I", "I"),
    KeySet(RightAB, LeftC) -> TextChord("J", "J"),

    KeySet(RightBC) -> TextChord("K", "K"),
    KeySet(RightBC, LeftA) -> TextChord("L", "L"),
    KeySet(RightBC, LeftB) -> TextChord("M", "M"),
    KeySet(RightBC, LeftC) -> TextChord("N", "N"),

    KeySet(LeftAB) -> TextChord("O", "O"),
    KeySet(LeftAB, RightA) -> TextChord("P", "P"),
    KeySet(LeftAB, RightB) -> TextChord("Q", "Q"),
    KeySet(LeftAB, RightC) -> TextChord("R", "R"),

    KeySet(LeftBC) -> TextChord("S", "S"),
    KeySet(LeftBC, RightA) -> TextChord("T", "T"),
    KeySet(LeftBC, RightB) -> TextChord("U", "U"),
    KeySet(LeftBC, RightC) -> TextChord("V", "V"),

    KeySet(LeftZ) -> TextChord("Th", "Th"),

    KeySet(RightZ) -> TextChord("W", "W"),
    KeySet(RightZ, LeftA) -> TextChord("X", "X"),
    KeySet(RightZ, LeftB) -> TextChord("Y", "Y"),
    KeySet(RightZ, LeftC) -> TextChord("Z", "Z"))

  val capsChords = globalChords ++: Map[KeySet, Chord](
    KeySet(LeftA) -> TextChord("A", "A"),
    KeySet(LeftA, RightB) -> TextChord("-", "-"),

    KeySet(LeftB) -> TextChord("B", "B"),
    KeySet(LeftB, RightC) -> TextChord(".", "."),

    KeySet(LeftC) -> TextChord("C", "C"),
    KeySet(LeftC, RightA) -> TextChord("!", "!"),

    KeySet(RightA) -> TextChord("D", "D"),
    KeySet(RightA, LeftB) -> TextChord("'", "'"),

    KeySet(RightB) -> TextChord("E", "E"),
    KeySet(RightB, LeftC) -> TextChord(",", ","),

    KeySet(RightC) -> TextChord("F", "F"),
    KeySet(RightC, LeftA) -> TextChord("/", "/"),

    KeySet(RightAB) -> TextChord("G", "G"),
    KeySet(RightAB, LeftA) -> TextChord("H", "H"),
    KeySet(RightAB, LeftB) -> TextChord("I", "I"),
    KeySet(RightAB, LeftC) -> TextChord("J", "J"),

    KeySet(RightBC) -> TextChord("K", "K"),
    KeySet(RightBC, LeftA) -> TextChord("L", "L"),
    KeySet(RightBC, LeftB) -> TextChord("M", "M"),
    KeySet(RightBC, LeftC) -> TextChord("N", "N"),

    KeySet(LeftAB) -> TextChord("O", "O"),
    KeySet(LeftAB, RightA) -> TextChord("P", "P"),
    KeySet(LeftAB, RightB) -> TextChord("Q", "Q"),
    KeySet(LeftAB, RightC) -> TextChord("R", "R"),

    KeySet(LeftBC) -> TextChord("S", "S"),
    KeySet(LeftBC, RightA) -> TextChord("T", "T"),
    KeySet(LeftBC, RightB) -> TextChord("U", "U"),
    KeySet(LeftBC, RightC) -> TextChord("V", "V"),

    KeySet(LeftZ) -> TextChord("TH", "TH"),

    KeySet(RightZ) -> TextChord("W", "W"),
    KeySet(RightZ, LeftA) -> TextChord("X", "X"),
    KeySet(RightZ, LeftB) -> TextChord("Y", "Y"),
    KeySet(RightZ, LeftC) -> TextChord("Z", "Z"))

  val numChords = globalChords ++: Map[KeySet, Chord](
    KeySet(LeftA) -> TextChord("1", "1"),
    KeySet(LeftA, RightB) -> TextChord("-", "-"),

    KeySet(LeftB) -> TextChord("2", "2"),
    KeySet(LeftB, RightC) -> TextChord(".", "."),

    KeySet(LeftC) -> TextChord("3", "3"),
    KeySet(LeftC, RightA) -> TextChord("!", "!"),

    KeySet(RightA) -> TextChord("4", "4"),
    KeySet(RightA, LeftB) -> TextChord("'", "'"),

    KeySet(RightB) -> TextChord("5", "5"),
    KeySet(RightB, LeftC) -> TextChord(",", ","),

    KeySet(RightC) -> TextChord("6", "6"),
    KeySet(RightC, LeftA) -> TextChord("?", "?"),

    KeySet(RightAB) -> TextChord("0", "0"),
    KeySet(RightAB, LeftA) -> TextChord("7", "7"),
    KeySet(RightAB, LeftB) -> TextChord("8", "8"),
    KeySet(RightAB, LeftC) -> TextChord("9", "9"),

    KeySet(RightBC) -> TextChord("#", "@"),
    KeySet(RightBC, LeftA) -> TextChord("@", "@"),
    KeySet(RightBC, LeftB) -> TextChord("`", "`"),
    KeySet(RightBC, LeftC) -> TextChord("&", "&"),

    KeySet(LeftAB) -> TextChord("+", "+"),
    KeySet(LeftAB, RightA) -> TextChord("%", "%"),
    KeySet(LeftAB, RightB) -> TextChord("=", "="),
    KeySet(LeftAB, RightC) -> TextChord("^", "^"),

    KeySet(LeftBC) -> TextChord("*", "*"),
    KeySet(LeftBC, RightA) -> TextChord("$", "$"),

    KeySet(LeftZ) -> TextChord(")", ")"),
    KeySet(LeftZ, RightA) -> TextChord("]", "]"),
    KeySet(LeftZ, RightB) -> TextChord(">", ">"),
    KeySet(LeftZ, RightC) -> TextChord("}", "}"),

    KeySet(RightZ) -> TextChord("(", "("),
    KeySet(RightZ, LeftA) -> TextChord("[", "["),
    KeySet(RightZ, LeftB) -> TextChord("<", "<"),
    KeySet(RightZ, LeftC) -> TextChord("{", "{"))
}
