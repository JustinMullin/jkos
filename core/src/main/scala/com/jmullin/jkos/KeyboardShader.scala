package com.jmullin.jkos

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShaderProgram

object KeyboardShader {
  lazy val default = {
    val program = new ShaderProgram(Gdx.files.internal(f"shader/default.vert"), Gdx.files.internal(f"shader/default.frag"))
    if(!program.isCompiled) {
      println(program.getLog)
    }
    program
  }

  lazy val pulse = {
    val program = new ShaderProgram(Gdx.files.internal(f"shader/default.vert"), Gdx.files.internal(f"shader/pulse.frag"))
    if(!program.isCompiled) {
      println(program.getLog)
    }
    program
  }

  lazy val post = {
    val program = new ShaderProgram(Gdx.files.internal(f"shader/default.vert"), Gdx.files.internal(f"shader/post.frag"))
    if(!program.isCompiled) {
      println(program.getLog)
    }
    program
  }

  def on()(implicit batch: SpriteBatch): Unit = {
    batch.flush()
    batch.setShader(pulse)
  }

  def off()(implicit batch: SpriteBatch): Unit = {
    batch.flush()
    batch.setShader(default)
  }
}
