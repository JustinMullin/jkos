package com.jmullin.jkos

import com.badlogic.gdx.Gdx
import com.jmullin.jkos.Chords._
import com.jmullin.jkos.entity.Keyboard

object Controller {
  class Mode(val map: KeyMap) {
    override def toString = getClass.getSimpleName.replaceAll("\\$", "")
  }
  object LowerAlpha extends Mode(Chords.lowercaseChords)
  object UpperAlpha extends Mode(Chords.uppercaseChords)
  object CapsAlpha extends Mode(Chords.capsChords)
  object Numeric extends Mode(Chords.numChords)

  case class KeyPress(id: KeyID, time: Long)

  var mode: Mode = LowerAlpha

  var keyReleases = Set[KeyPress]()
  var keysDown = Set[KeyID]()

  var lastPressShifted = false

  var deleteDelay = 0.2f
  var maxDeleteDelay = 0.2f

  var keyboardOutput = new KeyboardOutput {
    override def pressText(string: String): Unit = println(string)
    override def pressMeta(code: Int): Unit = println("META: " + code)
  }

  def down(id: KeyID): Unit = {
    keysDown += id
  }

  def up(id: KeyID): Unit = {
    keysDown -= id

    val now = System.currentTimeMillis()
    keyReleases += KeyPress(id, now)
    if(keysDown.isEmpty) {
      val chordKeys = keyReleases.filter(now - _.time < Chords.CHORD_TIME).map(_.id).toSet
      mode.map.find(_._1.ids.toSet == chordKeys).map(_._2).foreach {
        case c: MetaChord =>
          keyboardOutput.pressMeta(c.meta)
          updateModes(c.meta)
        case c: TextChord =>
          keyboardOutput.pressText(c.print)
          updateModes(-1)
      }
      keyReleases = Set()
    }
  }

  def updateModes(code: Int): Unit = {
    if(code != META_SHIFT) {
      lastPressShifted = false
    }

    val oldMode = mode

    code match {
      case META_SHIFT =>
        mode match {
          case LowerAlpha => lastPressShifted = true; mode = UpperAlpha
          case UpperAlpha => if(lastPressShifted) {
            mode = CapsAlpha
          } else {
            mode = LowerAlpha
          }
          lastPressShifted = false
          case CapsAlpha => mode = LowerAlpha
          case Numeric => Unit
        }
        Gdx.app.log("jkos", "Mode: " + mode)
      case META_NUM_SHIFT =>
        mode match {
          case LowerAlpha | UpperAlpha | CapsAlpha => mode = Numeric
          case Numeric => mode = LowerAlpha
        }
        Gdx.app.log("jkos", "Mode: " + mode)
      case META_FAST_DELETE => keyboardOutput.pressMeta(KeyEvent.KEYCODE_DEL)
      case META_SWITCH_KEYSET => Unit
      case _ => if(mode == UpperAlpha) mode = LowerAlpha
    }

    if(mode != oldMode) {
      Keyboard.buttons.foreach(_.refreshLabel(mode))
      Keyboard.modeLabel.setText(mode match {
        case LowerAlpha => "abc"
        case UpperAlpha => "Abc"
        case CapsAlpha => "ABC"
        case Numeric => "123"
      })
    }
  }

  def tick(delta: Float): Unit = {
    var fastDeleted = false

    val currentKeys = keysDown.toSet
    mode.map.find(_._1.ids.toSet == currentKeys).map(_._2).foreach {
      case c: MetaChord =>
        if(c.meta == META_FAST_DELETE) {
          deleteDelay -= delta
          if(deleteDelay <= 0) {
            keyboardOutput.pressMeta(KeyEvent.KEYCODE_DEL)
            deleteDelay = maxDeleteDelay
            maxDeleteDelay -= 0.025f
            if(maxDeleteDelay < 0.01f) maxDeleteDelay = 0.01f
          }
          fastDeleted = true
        }
      case _ => Unit
    }

    if(!fastDeleted) {
      maxDeleteDelay = 0.2f
      deleteDelay = maxDeleteDelay
    }
  }
}
