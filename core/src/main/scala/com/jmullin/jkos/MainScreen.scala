package com.jmullin.jkos

import com.jmullin.drifter.DrifterScreen
import com.jmullin.jkos.entity.Keyboard

class MainScreen extends DrifterScreen {
  override def initialActors = List(Keyboard)
}
