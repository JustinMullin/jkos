package com.jmullin.jkos

trait KeyboardOutput {
  def pressText(string: String)
  def pressMeta(code: Int)
}
