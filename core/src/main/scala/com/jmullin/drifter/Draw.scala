package com.jmullin
package drifter

import com.badlogic.gdx.graphics.{Color, Texture}
import com.badlogic.gdx.graphics.g2d.{Batch, Sprite}
import com.badlogic.gdx.math.Vector2

object Draw {
  def sprite(sprite: Sprite, v: Vector2, size: Vector2)(implicit batch: Batch) = {
    sprite.setBounds(v.x, v.y, size.x, size.y)
    sprite.draw(batch)
  }

  def texture(texture: Texture, v: Vector2, size: Vector2)(implicit batch: Batch) = {
    batch.setColor(Color.RED)
    batch.draw(texture, v.x, v.y, size.x, size.y)
  }
}