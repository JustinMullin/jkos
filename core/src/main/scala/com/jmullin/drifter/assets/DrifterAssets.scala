package com.jmullin
package drifter.assets

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.audio.{Music, Sound}
import com.badlogic.gdx.graphics.g2d.{BitmapFont, Sprite, TextureAtlas}
import com.badlogic.gdx.graphics.{Pixmap, Texture}
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.reflect.{ClassReflection, Field}

object DrifterAssets {
  val prefixMap = Map[Class[_], AssetType](
    classOf[Texture] -> AssetType("image/", ".png"),
    classOf[BitmapFont] -> AssetType("font/", ".fnt"),
    classOf[Sound] -> AssetType("sound/", ".wav"),
    classOf[Music] -> AssetType("music/", ".ogg"),
    classOf[Pixmap] -> AssetType("pixmap/", ".pix"),
    classOf[Skin] -> AssetType("skin/", ".skin")
  )
}

class DrifterAssets {
  import com.jmullin.drifter.assets.DrifterAssets._

  val manager = new AssetManager

  var atlas: Option[TextureAtlas] = None

  def fields = {
    ClassReflection.getDeclaredFields(this.getClass)
  }

  def load() {
    manager.load("atlas/jkos.atlas", classOf[TextureAtlas])

    for(field <- fields) {
      if(prefixMap.contains(field.getType)) {
        manager.load(getAssetPath(field), field.getType)
      }
    }
  }

  def finishLoading() {
    manager.finishLoading()
  }

  def populate() {
    atlas = Some(manager.get("atlas/jkos.atlas", classOf[TextureAtlas]))

    for(field <- fields) {
      if(prefixMap.contains(field.getType)) {
        val path = getAssetPath(field)

        field.setAccessible(true)
        field.set(this, manager.get(path, field.getType))
      }
    }

    for(field <- fields) {
      if(field.getType == classOf[Sprite]) {
        if(atlas.isEmpty) {
          throw new RuntimeException("No texture atlas loaded to pull sprite '" + field.getName + "' from.  " +
            "Make sure you have a TextureAtlas asset annotated with PrimaryAtlas.")
        }

        field.setAccessible(true)
        field.set(this, atlas.get.createSprite(field.getName))
      }
    }
  }

  def getAssetPath(field: Field) = {
    prefixMap(field.getType).pathPrefix + field.getName + prefixMap(field.getType).fileSuffix
  }

  def dispose() {
    atlas.map(_.dispose)
  }
}