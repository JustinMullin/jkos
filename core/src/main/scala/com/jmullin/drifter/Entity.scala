package com.jmullin
package drifter

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor

trait Entity extends Actor {
  override def draw(batch: Batch, parentAlpha: Float): Unit = {
    super.draw(batch, parentAlpha)

    render(batch)
  }

  def render(implicit batch: Batch)
}
