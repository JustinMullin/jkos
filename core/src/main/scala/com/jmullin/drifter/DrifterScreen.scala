package com.jmullin
package drifter

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.graphics.{FPSLogger, GL20}
import com.badlogic.gdx.math.{Vector2, Vector3}
import com.badlogic.gdx.scenes.scene2d.{Actor, Stage}
import com.badlogic.gdx.{Gdx, Screen}
import com.jmullin.jkos.KeyboardShader

abstract class DrifterScreen extends Screen {
  val shapeRenderer = new ShapeRenderer

  val size: Vector2 = new Vector2
  val mV: Vector3 = new Vector3
  val center: Vector2 = new Vector2

  var stage = new Stage()
  val postBatch = new SpriteBatch(1, KeyboardShader.post)

  val fps = new FPSLogger

  var time = 0f

  initialActors.map(stage.addActor)

  def initialActors: List[Actor]

  def render(delta: Float) {
    Gdx.gl.glClearColor(0.05f, 0.1f, 0.075f, 1)
    Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT)

    Gdx.gl.glEnable(GL20.GL_BLEND)
    Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)

    center.set(Gdx.graphics.getWidth/2.0f, Gdx.graphics.getHeight/2.0f)

    stage.act(Gdx.graphics.getDeltaTime)

    //val fbo = new FrameBuffer(Format.RGBA8888, gameW, gameH, false)
    //fbo.begin()

    stage.draw()

    //fbo.end()

    time += delta

    Gdx.gl.glDisable(GL20.GL_BLEND)

    /*postBatch.begin()
    KeyboardShader.post.setUniformf("time", time)
    postBatch.draw(fbo.getColorBufferTexture, 0, gameH, gameW, -gameH)
    postBatch.end()*/

    fps.log()
  }

  def resize(width: Int, height: Int) {
    size.set(width, height)
    stage.getViewport.update(width, height, true)
  }

  def show() {
    Gdx.input.setInputProcessor(stage)
  }

  def dispose() {
    stage.dispose()
  }

  def hide() {}
  def pause() {}
  def resume() {}
}