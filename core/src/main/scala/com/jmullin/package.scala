package com

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color

import scala.math._
import scala.util.Random

package object jmullin {
  // VECTORS
  type Vector2 = com.badlogic.gdx.math.Vector2
  type V2 = Vector2
  def V2(x: Float, y: Float) = new V2(x, y)

  type C = Color
  def C(r: Float, g: Float, b: Float) = new Color(r, g, b, 1.0f)

  def origin = V2(0, 0)
  def unit = V2(1, 1)

  val orthogonal = for(x <- -1 to 1; y <- -1 to 1; if abs(x)+abs(y)==1) yield V2(x, y)
  val adjacent = for(x <- -1 to 1; y <- -1 to 1; if abs(x)+abs(y)!=0) yield V2(x, y)

  // GDX

  def mouseX = Gdx.input.getX
  def mouseY = Gdx.input.getY

  def gameW = Gdx.graphics.getWidth
  def gameH = Gdx.graphics.getHeight

  def frameDelta = Gdx.graphics.getDeltaTime

  // RANDOM
  lazy val rand = new Random(System.currentTimeMillis())

  def cos(n: Float) = math.cos(n).toFloat
  def sin(n: Float) = math.sin(n).toFloat
  def tan(n: Float) = math.tan(n).toFloat
  def floor(n: Float) = math.floor(n).toFloat
  def ceil(n: Float) = math.ceil(n).toFloat

  def probability(p: Double) = rand.nextDouble() <= p
  def rInt(n: Int) = rand.nextInt(n)
  def rIntRange(n:Int, m:Int) = n+rand.nextInt(m-n)
  def rFloat(n: Float) = rand.nextFloat()*n
  def rFloatRange(n: Float, m: Float) = n+rand.nextFloat()*(m-n)
  def rElement[T](s: Iterable[T]) = rand.shuffle(s).head
  def rColor(n: Float, m: Float) = new Color(rFloatRange(n, m), rFloatRange(n, m), rFloatRange(n, m), 1.0f)

  def log(str: String) = Gdx.app.log("Vela", str)
}
