#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP 
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;
uniform sampler2D u_texture;
uniform LOWP vec2 pulseDownV;
uniform LOWP float pulseDownAmount;
uniform LOWP vec2 pulseUpV;
uniform LOWP float pulseUpAmount;

void main() {
  gl_FragColor = v_color * texture2D(u_texture, v_texCoords);
}