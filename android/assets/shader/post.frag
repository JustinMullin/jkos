#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP 
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;
uniform sampler2D u_texture;

uniform LOWP float time;

void main() {
  vec2 direction = v_texCoords - vec2(0.5);
  float distance = distance(v_texCoords, vec2(0.5));

  vec2 offset = direction * (sin(distance * 80.0 - time*15.0) + 0.5) / 30.0;

  gl_FragColor = texture2D(u_texture, v_texCoords+offset);
}