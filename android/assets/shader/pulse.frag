#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP 
#endif

varying LOWP vec4 v_color;
varying vec2 v_texCoords;
uniform sampler2D u_texture;

uniform LOWP vec2 pulseDownV;
uniform LOWP float pulseDownAmount;
uniform LOWP vec2 pulseUpV;
uniform LOWP float pulseUpAmount;

void main() {
  vec4 tempCol = texture2D(u_texture, v_texCoords);

  vec2 tempC = pulseUpV;
  float tempD = pulseUpAmount;

  vec4 pulse = vec4(0.25, 0.5, 0.3, pulseDownAmount);

  if(length(pulseDownV - gl_FragCoord.xy) < 150.0-(pulseUpAmount*150.0)) {
    pulse += vec4(0.125, 0.25, 0.15, pulseUpAmount);
  }

  gl_FragColor = pulse;
}