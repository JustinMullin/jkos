package com.jmullin.jkos;

import android.view.View;
import com.badlogic.gdx.backends.android.surfaceview.ResolutionStrategy;

public class FillHResolutionStrategy implements ResolutionStrategy {
    private final int height;

    public FillHResolutionStrategy(int height) {
        this.height = height;
    }

    @Override
    public MeasuredDimension calcMeasures(int widthMeasureSpec, int heightMeasureSpec) {
        final int width = View.MeasureSpec.getSize(widthMeasureSpec);

        return new MeasuredDimension(width, height);
    }
}
