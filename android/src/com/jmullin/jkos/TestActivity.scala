package com.jmullin.jkos

import android.app.Activity
import android.os.Bundle
import com.jmullin.jkos.R

class TestActivity extends Activity {
  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.test)
  }
}